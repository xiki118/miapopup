(function ($) {
    jQuery.fn.miaPopup = function (options) {
        var firstClick = 0;
        var obj_click = $(this);
        var modal_form = $(options.popupName);
        options = $.extend({
            popupName: "",
            overlayColor: "#000",
            overlayOpacity: "0.8",
            modalCloseText: "x",
            beforeOpen: false,
            afterClose: false
        }, options);

        $(modal_form).after("<div class='overlay'></div>");
        $(modal_form).wrap("<div class='modal_form_wrapper'></div>");
        if (options.modalCloseText != "none") {
            $(modal_form).append("<span class='modal_close'>" + options.modalCloseText + "</span>");
        }
        var obj_close = $(modal_form).find("span.modal_close");
        var wrapper = $(modal_form).parent(".modal_form_wrapper");
        var overlay = $(wrapper).next(".overlay");

        overlay.css({
            "background-color": options.overlayColor,
            "opacity": options.overlayOpacity
        });

        $(document).on('click', obj_click.selector, function (e) {
            if (wrapper.css('display') != 'block') {
                if ($.isFunction(options.beforeOpen)) {
                    options.beforeOpen(this, modal_form);
                }
                wrapper.fadeIn();
                wrapper.find("div").fadeIn();
                overlay.fadeIn();
                firstClick = true;
                $(document).bind('click.myEvent', function (e) {
                    if (!firstClick && $(e.target).closest(options.popupName).length == 0) {
                        wrapper.fadeOut();
                        wrapper.find("div").fadeOut();
                        overlay.fadeOut();
                        $(document).unbind('click.myEvent');
                        if ($.isFunction(options.afterClose)) {
                            options.afterClose();
                        }
                    }
                    firstClick = false;
                });
            }
            e.preventDefault();
        });
        $(obj_close).bind("click", function () {
            if ($.isFunction(options.afterClose)) {
                options.afterClose();
            }
            $(wrapper).fadeOut();
            $(wrapper).find("div").fadeOut();
            $(overlay).fadeOut();
            $(document).unbind('click.myEvent');
            firstClick = false;
        });
    };
})(jQuery);